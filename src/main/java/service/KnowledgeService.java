package service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import model.TcmKnowledge;
import model.User;
import result.Result;

import java.util.List;

public class KnowledgeService {

    //供controller
    public static KnowledgeService knowledgeService = new KnowledgeService();

    //dao
    TcmKnowledge tcmKnowledgeDao = TcmKnowledge.tcmKnowledge;
    User userDao = User.me;


    //查询所有数据
    public  List<Record> selectAll(int limit, int page){
        List<Record> record=tcmKnowledgeDao.selectAll(limit,(page-1)*limit);
        return  record;
    }

    //所有记录数
    public Integer countRecord() {
        Integer  count=tcmKnowledgeDao.countRecord();
        return count;
    }


    //查询
    public List<Record> selectByName(String holderName) {
        List<Record> record = tcmKnowledgeDao.selectByName(holderName);
        return record;
    }

    //查询(通过projectName)
    public List<Record> selectByProjectName(String projectName){
        List<Record> record=tcmKnowledgeDao.selectByProjectName(projectName);
        return record;
    }


    //查询详情
    public List<Record> selectDetail(Integer id){
        List<Record> record=tcmKnowledgeDao.selectDetail(id);
        return record;
    }



    //插入
    public Boolean insertRecord(String holderName,String holderType,String sex,String birth, String nationality,String postaddress,String phone,
                                String email ,String postalcode,String projectName,String area,String holderIntroduction,String inheritTime, Integer inheritTimes,
                                String inheritCondition,String medicType, String contentDescription, String mainCharacter, String importantCharacter,String document) {
         Boolean b = tcmKnowledgeDao.insertRecord(holderName,holderType,sex,birth,nationality,postaddress,phone,email,postalcode,projectName,area,holderIntroduction,inheritTime,inheritTimes,inheritCondition,medicType,contentDescription,mainCharacter,importantCharacter,document);
         return b;

    }

    //修改
    public Boolean updateRecord(Integer id,String holderName,String sex,String birth, String nationality,String postaddress,String phone,
                                String email ,String postalcode,String projectName,String area,String holderIntroduction,String inheritTime, Integer inheritTimes,
                                String inheritCondition,String medicType, String contentDescription, String mainCharacter, String importantCharacter){
        return tcmKnowledgeDao.updateRecord(id,holderName,sex,birth,nationality,postaddress,phone,email,postalcode,projectName,area,holderIntroduction,inheritTime,inheritTimes,inheritCondition,medicType,contentDescription,mainCharacter,importantCharacter);
    }


    //删除
    public Integer deleteById(String id){
        return tcmKnowledgeDao.deleteById(id);
    }


    public Result login(String username, String password) {
       Result code = userDao.isPermitted(username,password);
       return code;
    }
}