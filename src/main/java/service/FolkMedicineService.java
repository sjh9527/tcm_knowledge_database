package service;

import com.jfinal.plugin.activerecord.Record;
import model.FolkMedicine;

import java.util.List;

public class FolkMedicineService {

    public  static   FolkMedicineService medicineService = new FolkMedicineService();

    FolkMedicine medicineDao=FolkMedicine.me;

    public List<Record> listMedicine(int limit, int page){
       List<Record> list= medicineDao.listMedicine(limit,(page-1)*limit);
       return  list;
    }

    public  long  countMedicine(){
       long count=medicineDao.countMedicine();
       return count;
    }

    public List<Record> selectByName(String name) {
       List<Record> records =  medicineDao.selectByName(name);
       return records;
    }

    public List<Record> selectByPrescriptionName(String prescriptionName) {
        List<Record> records =  medicineDao.selectByPrescriptionname(prescriptionName);
        return records;
    }
}
