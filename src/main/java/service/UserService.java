package service;

import com.jfinal.plugin.activerecord.Record;

import java.util.List;
import java.util.Map;

public interface UserService  {

    /**
     * 查询所有用户
     * @return
     */
     Map<String,Object> listUsers(Integer page, Integer limit);

    /**
     * 修改用户
     * @return
     */
    boolean updateUser(Record record);


    /**
     * 删除用户
     */
    Integer removeUsers(String[] ids);

    /**
     * 增加用户
     */
    boolean addUser(Record record);




}
