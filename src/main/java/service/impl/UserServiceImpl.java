package service.impl;

import com.jfinal.plugin.activerecord.Record;
import model.User;
import service.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserServiceImpl implements UserService {
    public  static  UserServiceImpl userService = new UserServiceImpl();
    User userDao = User.me;
    @Override
    public Map<String,Object> listUsers(Integer page, Integer limit) {
        Map<String,Object> map = new HashMap<>();
        Long count=userDao.countUser();
        List<Record> records=userDao.listUsers((page-1)*limit,limit);
        map.put("count",count);
        map.put("list",records);
        return map;
    }

    @Override
    public boolean updateUser(Record record) {
        return userDao.updateUser(record);
    }

    @Override
    public Integer removeUsers(String[] ids) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        for (int i=0;i<ids.length;i++)
        {
            sb.append(ids[i]);
            if (i != ids.length -1)
                sb.append(',');
        }
        sb.append(')');
        return userDao.removeUser(sb.toString());
    }

    @Override
    public boolean addUser(Record record) {
        return userDao.addUser(record);
    }

    public boolean nameExisted(String username) {
        boolean flag= true;
      Record record = userDao.selectByUsername(username);
      if (record==null)
          flag =false;

      return flag;
    }
}
