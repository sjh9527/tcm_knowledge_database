package config;


import com.jfinal.config.*;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.template.Engine;
import controller.FolkMedicineController;
import controller.KnowledgeController;
import controller.UserController;
import model.FolkMedicine;
import model.TcmKnowledge;
import model.User;

public class MyConfig extends JFinalConfig {

    @Override
    public void configConstant(Constants constants) {
        loadPropertyFile("jdbc.properties");
        constants.setDevMode(getPropertyToBoolean("devMode",false));
//        constants.setBaseUploadPath("upload");

    }

    @Override
    public void configRoute(Routes routes) {
        routes.add("/", KnowledgeController.class);
        routes.add("/folk", FolkMedicineController.class);
        routes.add("/user", UserController.class);
    }

    @Override
    public void configEngine(Engine engine) {


    }

    @Override
    public void configPlugin(Plugins me) {
        C3p0Plugin c3p0Plugin=new C3p0Plugin(getProperty("jdbcUrl"),getProperty("user"),getProperty("password").trim(),getProperty("driverClass").trim());
        me.add(c3p0Plugin);
        ActiveRecordPlugin arp=new ActiveRecordPlugin(c3p0Plugin);
        me.add(arp);
        arp.addMapping("folk_medicine", FolkMedicine.class);
        arp.addMapping("tcm_knowledge", TcmKnowledge.class);
        arp.addMapping("user", User.class);

    }

    @Override
    public void configInterceptor(Interceptors interceptors) {
//        interceptors.add(new MyInterceptor());
    }

    @Override
    public void configHandler(Handlers handlers) {

    }
}
