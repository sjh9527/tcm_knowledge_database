package action;


import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.plugin.activerecord.Record;

import javax.servlet.http.HttpSession;

public class UserInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation invocation) {
        HttpSession session = invocation.getController().getSession();

        //获取session中的对象
        Record user = (Record) session.getAttribute("user");
        String userType=user.get("user_type");
        if ( user !=  null && "管理员".equals(userType))
            invocation.invoke();
        else
            invocation.getController().redirect("/index");
    }
}
