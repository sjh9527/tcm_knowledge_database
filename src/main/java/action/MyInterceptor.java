package action;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.plugin.activerecord.Record;
import model.User;

import javax.servlet.http.HttpSession;

public class MyInterceptor implements Interceptor {
    @Override
    public void intercept(Invocation invocation) {
        //获取session
       HttpSession session = invocation.getController().getSession();

       //获取session中的对象
        Record user = (Record) session.getAttribute("user");
        if ( user !=  null)
            invocation.invoke();
        else
            invocation.getController().redirect("/index");
    }
}
