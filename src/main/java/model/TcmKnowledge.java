package model;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

public class TcmKnowledge extends Model<TcmKnowledge>{

    //供service
    public static  TcmKnowledge tcmKnowledge = new TcmKnowledge();


    //查询所有数据(含分页)
    public  List<Record> selectAll(int limit, int page){
        List<Record> records=Db.find(" select * from tcm_knowledge where is_valid=1 order by id desc limit "+page +","+limit +" ");
        return  records;
    }

    //所有记录数
    public Integer countRecord() {
       Integer  count=Db.queryInt("select count(1) from tcm_knowledge where is_valid=1");
        return count;
    }



    //查询(通过holderName)
    public List<Record> selectByName(String holderName){
        List<Record> record= Db.find(" select * from tcm_knowledge where is_valid=1 and holder_name like "+'\''+'%'+holderName+'%'+'\'');
        return record;
    }

    //查询(通过projectName)
    public List<Record> selectByProjectName(String projectName){
        List<Record> record= Db.find(" select * from tcm_knowledge where is_valid=1 and project_name like "+'\''+'%'+projectName+'%'+'\'');
        return record;
    }



    //详情 (通过 id)
    public List<Record> selectDetail(Integer id){
        List<Record> record=Db.find(" select * from tcm_knowledge where is_valid=1 and id= "+id);
        return record;
    }

    //插入
    public Boolean insertRecord(String holderName,String holderType,String sex,String birth, String nationality,String postaddress,String phone,
      String email ,String postalcode,String projectName,String area,String holderIntroduction,String inheritTime, Integer inheritTimes,
      String inheritCondition,String medicType, String contentDescription, String mainCharacter, String importantCharacter,String document) {
        System.out.println("sex:"+sex);
        Record record=new Record();
        record.set("holder_name",holderName);record.set("holder_type",holderType);
        record.set("sex",sex);record.set("birth",birth);
        record.set("nationality",nationality);record.set("postaddress",postaddress);record.set("phone",phone);
        record.set("email",email);record.set("postalcode",postalcode);record.set("project_name",projectName);
        record.set("area",area);record.set("holder_introduction",holderIntroduction);record.set("inherit_time",inheritTime);
        record.set("inherit_times",inheritTimes);record.set("inherit_condition",inheritCondition);record.set("type",medicType);
        record.set("content_description",contentDescription);
        record.set("main_character",mainCharacter);
        record.set("important_character",importantCharacter);
        record.set("document",document);
      return  Db.save("tcm_knowledge",record);
    }

    //修改
    public Boolean updateRecord(Integer id,String holderName,String sex,String birth, String nationality,String postaddress,String phone,
                                String email ,String postalcode,String projectName,String area,String holderIntroduction,String inheritTime, Integer inheritTimes,
                                String inheritCondition,String medicType, String contentDescription, String mainCharacter, String importantCharacter){
//     Integer  record =  Db.update("update tcm_knowledge set "+fieldName+"="+fieldValue+"where id="+id);
        Record record=new Record();
        record.set("id",id);
        record.set("holder_name",holderName);
        record.set("sex",sex);record.set("birth",birth);
        record.set("nationality",nationality);record.set("postaddress",postaddress);record.set("phone",phone);
        record.set("email",email);record.set("postalcode",postalcode);record.set("project_name",projectName);
        record.set("area",area);record.set("holder_introduction",holderIntroduction);record.set("inherit_time",inheritTime);
        record.set("inherit_times",inheritTimes);record.set("inherit_condition",inheritCondition);record.set("type",medicType);
        record.set("content_description",contentDescription);
        record.set("main_character",mainCharacter);
        record.set("important_character",importantCharacter);
        System.out.println(id);
        System.out.println(holderName);
        System.out.println(birth);
        System.out.println(mainCharacter);
        System.out.println(importantCharacter);
        System.out.println(mainCharacter);
        System.out.println(area);
//        record.set("document",document);
     return  Db.update("tcm_knowledge",record);
    }

    //删除( 通过id 把is_valid设为0)
    public Integer deleteById(String id){
//        Record record;
//        record=Db.findById("tcm_knowledge",id).set("is_valid",0);
//        return  Db.update("tcm_knowledge",record);

        return  Db.update(" update tcm_knowledge  set  is_valid=0  where  id  in "+ id );
    }



}
