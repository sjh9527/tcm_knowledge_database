package model;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

public class FolkMedicine extends Model<FolkMedicine> {

    public static  FolkMedicine  me  = new  FolkMedicine();


    public List<Record> listMedicine(int limit, int page) {
//        Db.find("select name,sex,birth,concat(province,'省',city,'市',town) as address");
         List<Record> list=Db.find("select * " +
                                       "from folk_medicine " +
                                       "limit "+page +","+limit
                                       );
          return list;
    }


    public long countMedicine() {
        long count=Db.queryLong("select count(1) " +
                                    "from folk_medicine");
        return count;
    }

    public List<Record> selectByName(String name) {
        List<Record> records=Db.find("select * " +
                     "from folk_medicine " +
                     "where name like '%"+name+"%'");
        return records;
    }

    public  List<Record> selectByPrescriptionname(String prescriptionName){
        List<Record> records = Db.find("select * " +
                                            "from folk_medicine " +
                                            " where prescriptionname like '%"+prescriptionName+"%'");
        return records;
    }
}
