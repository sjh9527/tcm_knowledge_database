package controller;

import action.MyInterceptor;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Record;
import model.User;
import result.Result;
import service.FolkMedicineService;

import java.util.List;

@Before(MyInterceptor.class)
public class FolkMedicineController extends Controller {

    FolkMedicineService service =FolkMedicineService.medicineService;

    public  void index(){
        render("/folkMedicine/folkMedicine.html");
    }

    public  void admin(){
        render("/folkMedicine/admin.html");
    }

    public  void normalAdmin(){
        render("/normalUser/normalUserFolkAdmin.html");
    }

    public void userJudge(){
        Record user=getSessionAttr("user");
        String userType=user.get("user_type");
        if ("管理员".equals(userType)){
            render("/folkMedicine/admin.html");
        }else if ("普通用户".equals(userType)){
            render("/normalUser/normalUserFolkAdmin.html");
        }else{
            index();
        }
    }
    public void listMedicine(){
        int limit =  getParaToInt("limit");
        int page =  getParaToInt("page");
        long count=service.countMedicine();
        List<Record> list=service.listMedicine(limit,page);
        renderJson(Result.layui(count,list));
    }

    public  void selectByHolderName(){
       String name = getPara("name");
       List<Record> records =service.selectByName(name);
       renderJson(Result.layui(records.size(),records));

    }

    public  void selectByPrescriptionName(){
        String name = getPara("prescriptionname");
        List<Record> records = service.selectByPrescriptionName(name);
        renderJson(Result.layui(records.size(),records));
    }





}
