package result;

/**
 * 结果处理集
 */
public enum  CodeMsg {
    SUCCESS(1,"成功"),

    //数据库异常

    //传统知识模块异常
    SELECT_ERROR(2,"无查询结果!"),
    INSERT_ERROR(2,"插入失败!"),
    UPDATE_ERROR(2,"更新失败!"),
    DELETE_ERROR(2,"删除失败!"),
    UPLOAD_FAIL(5,"传统中医药附件上传失败"),

    //民间医药模块异常


    //用户模块异常
    UPDATE_USER_ERROR(101,"编辑用户失败"),
    ADD_USER_ERROR(102,"添加用户失败"),
    REMOVE_USERS_ERROR(103,"删除多个用户失败"),
    REMOVE_ADMIN_ERROR(104,"您不能删除管理员"),
    USERNAME_EXISTED(105,"用户名已存在")
    ;


    private int code;
    private String msg;


    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    CodeMsg(int code,String msg)
    {
        this.code = code;
        this.msg = msg;
    }
}
