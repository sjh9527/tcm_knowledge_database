package result;

public  class Result<T> {

    private  int code;
    private  String msg;
    private  long count;
    private  T data;

    public int getCode() {
        return code;
    }


    public String getMsg() {
        return msg;
    }

    public long getCount() {
        return count;
    }


    public T getData() {
        return data;
    }

    private Result(T data)
    {
        this.code = 0;
        this.msg = "success";
        this.data=data;
    }

    private Result(CodeMsg codeMsg)
    {
//        if (codeMsg == null)
//            return;
        this.code=codeMsg.getCode();
        this.msg=codeMsg.getMsg();
    }

    private Result(long count,T data)
    {
        this.code = 0;
        msg ="layui数据接口";
        this.count = count;
        this.data = data;
    }

    //
    private Result(Integer code,String msg){
        this.code=code;
        this.msg=msg;
    }

    //自定返回状态
    public static <T> Result<T> result(Integer code ,String msg){
        return  new Result<T>(code,msg);
    }


    /**
     * 成功时候的调用
     *
     */
    public  static  <T> Result<T>  success(CodeMsg codeMsg)
    {

        return  new Result<T>(codeMsg);
    }

    /**
     * 失败的时候调用
     * @param <T>
     * @return
     */
    public static  <T> Result<T> error(CodeMsg cm)
    {
        return  new Result<T>(cm);
    }



    /**
     * layui返回的接口
     */


    /**
     * layui的调用
     */
    public static   <T>Result<T>  layui(long count,T data){ return   new Result(count,data);}


}
